Feature: Home Page

Background:
Given I am on the home page

Scenario: Click on the recipe detail 
	And I click on the Paleo Avocado Tuna Salad link
	Then the title will Paleo Avocado Tuna Salad
	
Scenario: Click on the meal planner tab  
	And I click on the View Meals link
	Then the title will User Name:
	

Scenario: Click on the saved recipes tab  
	And I click on the View Saved Recipes link
	Then the title will User Name:	

Scenario: Click on the Login tab  
	And I click on the Login link
	Then the title will User Name:	

Scenario: Click on the Sign up tab  
	And I click on the Sign Up link
	Then the title will First Name:	