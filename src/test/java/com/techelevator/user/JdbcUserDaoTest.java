package com.techelevator.user;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.user.User;
import com.techelevator.user.UserDao;

public class JdbcUserDaoTest {
	
	private static UserDao userDao;
	private static SingleConnectionDataSource dataSource;
	
	/* Before any tests are run, this method initializes the datasource for testing. */
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/capstone");
		dataSource.setUsername("capstone_appuser");
		dataSource.setPassword("capstone_appuser1");
		/* The following line disables autocommit for connections 
		 * returned by this DataSource. This allows us to rollback
		 * any changes after each test */
		dataSource.setAutoCommit(false);
		
		userDao = new JdbcUserDao(dataSource);
	}
	
	/* After all tests have finished running, this method will close the DataSource */
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}

	/* After each test, we rollback any changes that were made to the database so that
	 * everything is clean for the next test */
	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	/* This method provides access to the DataSource for subclasses so that 
	 * they can instantiate a DAO for testing */
	protected DataSource getDataSource() {
		return dataSource;
	}
	
	@Test
	public void returns_true_if_userName_and_password_match() {
		userDao.createNewUser("spiderman", "uncleBen", "Peter", "Parker");
		boolean result = userDao.searchForUsernameAndPassword("spiderman", "uncleBen");
		assertThat(result, equalTo(true));
	}
	
	@Test
	public void returns_false_if_userName_does_not_exist() {
		userDao.createNewUser("spiderman", "uncleBen", "Peter", "Parker");
		boolean result = userDao.searchForUsernameAndPassword("antman", "uncleBen");
		assertThat(result, equalTo(false));
	}
	
	@Test
	public void returns_false_if_userName_exists_but_password_is_incorrect() {
		userDao.createNewUser("spiderman", "uncleBen", "Peter", "Parker");
		boolean result = userDao.searchForUsernameAndPassword("spiderman", "uncleBob");
		assertThat(result, equalTo(false));
	}
	
	@Test
	public void quicky() {
		boolean result = userDao.searchForUsernameAndPassword("Tyler", "password");
		assertThat(result, equalTo(true));
	}

}
