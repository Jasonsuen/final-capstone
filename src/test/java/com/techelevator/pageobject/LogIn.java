package com.techelevator.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogIn implements PageObject {

	private WebDriver webDriver;
	
	public LogIn(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}

	@Override
	public String getPageTitle(){
		WebElement title = webDriver.findElement(By.cssSelector(".title"));
		return title.getText();
	}
	
	public LogIn enterUsername(String username){
		 webDriver.findElement(By.id("username")).sendKeys(username);
		 return this;  
	}
	
	public LogIn enterPassword(String password){
		webDriver.findElement(By.id("password")).sendKeys(password);
		return this;  
	}
	
	public HomePage submitLogin() {
		webDriver.findElement(By.xpath("/html/body/div/div/form/button")).click();
		return new HomePage(webDriver);
	}

	@Override
	public String getCurrentUser() {
		WebElement currentUser = webDriver.findElement(By.id("currentUser"));
		return currentUser.getText();
	}

}
