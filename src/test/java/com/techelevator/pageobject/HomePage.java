package com.techelevator.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;



public class HomePage {

	private WebDriver webDriver;
	
	public HomePage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public List<WebElement> getRecipes(){
		return webDriver.findElements(By.className("recipes"));
	}
	public RecipeDetail clickRecipeDetailLink(){
		WebElement PaleoAvocadoTunaSaladLink = webDriver.findElement(By.linkText("Paleo Avocado Tuna Salad"));
		PaleoAvocadoTunaSaladLink.click();
		return new RecipeDetail(webDriver);
	}
	
	public MealPlanner clickMealPlannerLink(){
		WebElement MealPlannerLink = webDriver.findElement(By.linkText("View Meals"));
		MealPlannerLink.click();
		return new MealPlanner(webDriver);
	}
	
	
	public SavedRecipes clickSavedRecipes(){
		WebElement SavedRecipesLink = webDriver.findElement(By.linkText("View Saved Recipes"));
		SavedRecipesLink.click();
		return new SavedRecipes(webDriver);
	}
	
//	public AboutUs clickAboutUs(){
//		WebElement AboutUsLink = webDriver.findElement(By.linkText("About Us"));
//		AboutUsLink.click();
//		return new AboutUs(webDriver);
//	}
	public LogIn clickLogIn(){
		WebElement LogInLink = webDriver.findElement(By.linkText("Login"));
		LogInLink.click();
		return new LogIn(webDriver);
	}
	
	public SignUp clickSignUp(){
		WebElement SignUpLink = webDriver.findElement(By.linkText("Sign Up"));
		SignUpLink.click();
		return new SignUp(webDriver);
	}
	
	public PageObject clickLink(String linkText){
		WebElement pageLink = webDriver.findElement(By.id(linkText));
		pageLink.click();
		
		if(linkText.equals("Paleo Avocado Tuna Salad")) {
			return new RecipeDetail(webDriver);
		}else if(linkText.equals("View Meals")){
			return new MealPlanner(webDriver);
		}else if(linkText.equals("View Saved Recipes")){
			return new SavedRecipes(webDriver);
		}else if(linkText.equals("Login")){
			return new LogIn(webDriver);
		}else if(linkText.equals("Sign Up")){
			return new LogIn(webDriver);
//		}else if(linkText.equals("About Us")){
//			return new AboutUs(webDriver);	
		}else{
			return null;
		}
	}
	
}