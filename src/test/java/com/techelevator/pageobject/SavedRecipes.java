package com.techelevator.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SavedRecipes implements PageObject {

	private WebDriver webDriver;
	
	public SavedRecipes(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}

	@Override
	public String getPageTitle(){
		WebElement title = webDriver.findElement(By.cssSelector(".title"));
		return title.getText();
	}
	

	@Override
	public String getCurrentUser() {
		// TODO Auto-generated method stub
		return null;
	}
}
