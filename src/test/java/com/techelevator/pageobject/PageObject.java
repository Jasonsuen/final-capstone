package com.techelevator.pageobject;

public interface PageObject {

	String getPageTitle();
	String getCurrentUser();
	
}
