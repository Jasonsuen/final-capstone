package com.techelevator.cukes;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.pageobject.HomePage;
import com.techelevator.pageobject.LogIn;
import com.techelevator.pageobject.PageObject;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

@Component
public class HomepageLoginStepTest {

		
		private WebDriver webDriver;
		private HomePage homePage;
		private LogIn loginPage;
		private PageObject page;

	@Autowired 
	public HomepageLoginStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.homePage = new HomePage(webDriver);
		
	}
	@Given ("^I am on the homepage$")
	public void i_am_on_the_homepage_login() throws Throwable {
		webDriver.get("http://localhost:8080/capstone/");
		homePage = new HomePage(webDriver);
		
		page = null;
	}
	
	@Given("^I click on Login$")
	public void i_click_on_the_login_link() throws Throwable {
		loginPage = homePage.clickLogIn();
	}
	
	@Given("^I enter the username as (.+)$")
	public void i_enter_username(String username) throws Throwable {
		  page = loginPage.enterUsername(username);
	}
	
	@Given("^I enter the password as (.+)$")
	public void i_enter_pssword(String password) throws Throwable {
		  page = loginPage.enterPassword(password);
	}
	
	@Given("^I login$")
	public void i_login() throws Throwable {
		  homePage = loginPage.submitLogin();
	}
	
	@Then("^the currentUser will be (.+)$")
	public void the_page_title_is(String username) throws Throwable {
			Assert.assertEquals(username, page.getCurrentUser());
		}
}
