package com.techelevator.directions;

import java.util.List;

public interface DirectionsDao {

	public List<Directions> getAllDirections();
	public Directions getDirectionsById(int directionsId);
	public void createDirections(String directions);
	public void updateDirections(int recipeId, String directions);
}
