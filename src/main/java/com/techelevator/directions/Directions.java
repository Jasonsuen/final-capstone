package com.techelevator.directions;

public class Directions {

	private int directionsId;
	private String directions;
	
	public int getDirectionsId() {
		return directionsId;
	}
	public void setDirectionsId(int directionsId) {
		this.directionsId = directionsId;
	}
	public String getDirections() {
		return directions;
	}
	public void setDirections(String directions) {
		this.directions = directions;
	}
}
