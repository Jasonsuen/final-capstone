package com.techelevator.directions;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcDirectionsDao implements DirectionsDao {
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcDirectionsDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Directions> getAllDirections() {
		List<Directions> allDirections = new ArrayList<>();
		String sqlSelectAllDirections = "SELECT * FROM directions";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllDirections);
		while(results.next()) {
			Directions directions = new Directions();
			directions.setDirectionsId(results.getInt("directions_id"));
			directions.setDirections(results.getString("directions"));
			allDirections.add(directions);
		}
		return allDirections;
	}

	@Override
	public Directions getDirectionsById(int directionsId) {
		List<Directions> allDirections = getAllDirections();
		for (Directions directions : allDirections) {
			if(directions.getDirectionsId() == directionsId) {
				return directions;
			}
		}
		return null;
	}

	@Override
	public void createDirections(String directions) {
		String sqlInsertDirections = "INSERT INTO directions(directions) VALUES (?)";
		jdbcTemplate.update(sqlInsertDirections, directions);
	}

	@Override
	public void updateDirections(int recipeId, String directions) {
		String sqlUpdateDirections = "UPDATE directions SET directions=? WHERE directions_id=?";
		jdbcTemplate.update(sqlUpdateDirections, directions, recipeId);
	}

}
