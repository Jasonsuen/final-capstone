package com.techelevator.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.user.UserDao;


@Controller
@SessionAttributes("currentUser")
	public class AuthenticationController {

		private UserDao userDAO;

		@Autowired
		public AuthenticationController(UserDao userDAO) {
			this.userDAO = userDAO;
		}
		
		@RequestMapping(path="/login", method=RequestMethod.POST)
		public String login(Map<String, Object> model,
							HttpSession session,
							@RequestParam String username, 
							@RequestParam String password) {
			if(userDAO.searchForUsernameAndPassword(username, password)){
				session.invalidate();
				model.put("currentUser", username);
				System.out.println("Authed User");
				return "redirect:/"+username+"/userDashboard";
			} else {
				System.out.println("Auth Failed");
				return "redirect:/login";
			}
		}
		
		@RequestMapping(path="/logout", method=RequestMethod.GET)
 		public String logout(ModelMap model, HttpSession session) {
 			model.remove("currentUser");
 			session.removeAttribute("currentUser");
 			System.out.println("Logged out");
 			return "redirect:/";
 		}
	}