package com.techelevator.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import com.techelevator.user.UserDao;

@Controller
public class UserController {

	private UserDao userDAO;

	@Autowired
	public UserController(UserDao userDAO) {
		this.userDAO = userDAO;
	}

	@RequestMapping(path="/users/new", method=RequestMethod.GET)
	public String displayNewUserForm() {
		return "redirect:/signUp";
	}
	
	@RequestMapping(path="/signUp", method=RequestMethod.POST)
	public String createUser(@RequestParam String username, @RequestParam String password, @RequestParam String firstName,@RequestParam String lastName) {
		try {
			userDAO.createNewUser(username, password, firstName, lastName);
			return "redirect:/login";
		} catch (DuplicateKeyException e) {
			return "redirect:/login";
		}
	}
	
	@RequestMapping(path="/users/{username}", method=RequestMethod.GET)
	public String displayDashboard(Map<String, Object> model, @PathVariable String username) {
		return "userDashboard";
	}
	
	@RequestMapping(path="/changePassword", method=RequestMethod.GET)
	public String displayChangePasswordForm() {
		
		
	
		return "changePassword";
	}
	
	@RequestMapping(path="/users/{username}/changePassword", method=RequestMethod.POST)
	public String changePassword(@PathVariable String username, @RequestParam String password) {
		userDAO.changePassword(username, password);
		return "redirect:/userDashboard";
	}
	
	
	


	

}
