package com.techelevator.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.directions.DirectionsDao;
import com.techelevator.ingredients.Ingredients;
import com.techelevator.ingredients.IngredientsDao;
import com.techelevator.plan.PlanDao;
import com.techelevator.recipe.Recipe;
import com.techelevator.recipe.RecipeDao;
import com.techelevator.user.UserDao;



@Controller
public class HomeController {
	

	private  IngredientsDao ingredientsDao;
	private  DirectionsDao directionsDao;
	private  RecipeDao recipeDao;
	private	 UserDao userDao;
	private  PlanDao planDao;
	
	
	@Autowired
	public HomeController(IngredientsDao ingredientsDao, DirectionsDao directionsDao, RecipeDao recipeDao, UserDao userDao,  PlanDao planDao) {
		this.ingredientsDao = ingredientsDao;
		this.directionsDao = directionsDao;
		this.recipeDao = recipeDao;
		this.userDao = userDao;
		this.planDao = planDao;
	}
	
	@RequestMapping(path={"/", "/homePage"}, method=RequestMethod.GET)
	public String displayHomePage(HttpServletRequest request) {
		List<Recipe> recipe = recipeDao.getAllRecipes();
		request.setAttribute("recipe", recipe);
		return "homePage";
	}
	
	@RequestMapping(path={"/{username}", "/{username}/homePage"}, method=RequestMethod.GET)
	public String displayHomePageUser(HttpServletRequest request) {
		List<Recipe> recipe = recipeDao.getAllRecipes();
		request.setAttribute("recipe", recipe);
		return "homePage";
	}
	
	@RequestMapping(path={"recipeDetail"}, method=RequestMethod.GET)
	public String displayRecipeDetail(HttpServletRequest request) {
		try {
		String recipeIdParse = request.getParameter("recipeId");
		int recipeId = Integer.parseInt(recipeIdParse);
		request.setAttribute("recipe", recipeDao.getRecipeById(recipeId));
		request.setAttribute("directions", directionsDao.getDirectionsById(recipeId));
		request.setAttribute("ingredients", ingredientsDao.getIngredientsById(recipeId));
		request.setAttribute("user", userDao.getUsersByUsername(recipeDao.getRecipeById(recipeId).getUsername()));
			return "recipeDetail";
		} catch (DuplicateKeyException e){
			return "alreadyAdded";
		}
	}
	
	@RequestMapping(path={"/{username}/recipeDetail"}, method=RequestMethod.GET)
	public String displayRecipeDetailUser(HttpServletRequest request) {
		try {
		String recipeIdParse = request.getParameter("recipeId");
		int recipeId = Integer.parseInt(recipeIdParse);
		request.setAttribute("recipe", recipeDao.getRecipeById(recipeId));
		request.setAttribute("directions", directionsDao.getDirectionsById(recipeId));
		request.setAttribute("ingredients", ingredientsDao.getIngredientsById(recipeId));
		request.setAttribute("user", userDao.getUsersByUsername(recipeDao.getRecipeById(recipeId).getUsername()));
			return "recipeDetail";
		} catch (DuplicateKeyException e){
			return "alreadyAdded";
		}
	}
	
	@RequestMapping(path="/login", method=RequestMethod.GET)
	public String displayLoginForm(HttpServletRequest request) {
		return "login";
	}

	@RequestMapping(path="/{username}/userDashboard", method=RequestMethod.GET)
	public String displayUserDash(Map<String, Object> model, @PathVariable String username) {
		model.put("user", userDao.getUsersByUsername(username));
		model.put("count", userDao.totalSubmitsSavedToUser(username));
		return "userDashboard";
	}
	
	@RequestMapping(path="//userDashboard", method=RequestMethod.GET)
	public String redirectUserDash() {
		
		return "redirect:/login";
	}
	@RequestMapping(path="/signUp", method=RequestMethod.GET)
	public String displaySignup() {
		return "signUp";
	}

	

	@RequestMapping(path={"newRecipe"}, method=RequestMethod.GET)
	public String redirectDisplayNewRecipe(HttpServletRequest request) {
		return "redirect:/login";
	}
	
	@RequestMapping(path={"/{username}/newRecipe"}, method=RequestMethod.GET)
	public String displayNewRecipe(HttpServletRequest request) {
		return "newRecipe";
	}
	
	@RequestMapping(path={"aboutUs"}, method=RequestMethod.GET)
	public String displayAboutUs(HttpServletRequest request) {
		return "aboutUs";
	}
	
	@RequestMapping(path={"/{username}/aboutUs"}, method=RequestMethod.GET)
	public String displayAboutUsUser(HttpServletRequest request) {
		return "aboutUs";
	}
	
	@RequestMapping(path={"/{username}/newRecipe"}, method=RequestMethod.POST)
	public String enterNewRecipe(@PathVariable String username, @RequestParam String name, @RequestParam String description, @RequestParam int cookTime, @RequestParam int servings, @RequestParam String directions, @RequestParam String ingredients, HttpServletRequest request) throws FileNotFoundException, IOException {
		
		String healthyParam = request.getParameter("healthy");
		String paleoParam = request.getParameter("paleo");
		String vegetarianParam = request.getParameter("vegetarian");
		Boolean vegetarian;
		Boolean paleo;
		Boolean healthy;
		if (healthyParam == null) {
			healthy = false;
		} else {
			healthy = true;
		}
		if (paleoParam == null) {
			paleo = false;
		} else {
			paleo = true;
		}
		if (vegetarianParam == null) {
			vegetarian = false;
		} else {
			vegetarian = true;
		}
	
		recipeDao.createNewRecipe(name, description, cookTime, servings, healthy, paleo, vegetarian, username);
		ingredientsDao.createNewIngredients(ingredients);
		directionsDao.createDirections(directions);
		
		return "redirect:/addSuccessful";
	}
	
	@RequestMapping("/addSuccessful")
	public String showAddSuccessful() {
		
		return "addSuccessful";
	}
	
	@RequestMapping(path="/{username}/mealPlanner", method=RequestMethod.GET)
	public String displayMealPlanner(Map<String, Object> model, @PathVariable String username) {
		
		model.put("mealPlans", planDao.getPlansByUsername(username));
		
		return "mealPlanner";
	}
	
	@RequestMapping(path="/{username}/planPicker", method=RequestMethod.POST)
	public String pickPlan(Map<String, Object> model, @PathVariable String username, @RequestParam int recipeId) {
		model.put("mealPlans", planDao.getPlansByUsername(username));
		model.put("recipeId", recipeId);
		return "planPicker";
	}
	
	@RequestMapping(path="//planPicker", method=RequestMethod.POST)
	public String pickPlanRedirect() {
		
		return "redirect:/login";
	}
	
	@RequestMapping(path="/{username}/addRecipe", method=RequestMethod.GET)
	public String addRecipe(@PathVariable String username, @RequestParam int recipeId, @RequestParam int planId) {
		try {
			userDao.addRecipeToUsersPlan(username, recipeId, planId);
			return "redirect:/homePage";
		} catch (DuplicateKeyException e){
			return "alreadyAdded";
		}	
	}
	
	@RequestMapping(path="/{username}/mealPlanDeleted", method=RequestMethod.GET)
	public String deletePlan(@PathVariable String username, @RequestParam int mealPlanId) {
		userDao.removeEntirePlan(username, mealPlanId);
		return "mealPlanDeleted";
	}
	
	@RequestMapping(path="/{username}/mealPlanCleared", method=RequestMethod.GET)
	public String clearPlan(int mealPlanId) {
		userDao.removeAllRecipesFromPlan(mealPlanId);
		return "mealPlanCleared";
	}
	
	@RequestMapping(path="//mealPlanner", method=RequestMethod.GET)
	public String redirectMealPlanner() {
				
		return "redirect:/login";
	}
	
	@RequestMapping(path={"//newRecipe"}, method=RequestMethod.POST)
	public String redirectAddRecipe() {
		return "redirect:/login";
	}
	
	@RequestMapping(path="/{username}/mealPlannerRecipeList", method=RequestMethod.GET)
	public String displayMealPlanRecipes(Map<String, Object> model, @PathVariable String username, @RequestParam int planId) {
		model.put("mealPlanRecipes", recipeDao.getRecipesByPlan(planId));
		model.put("mealPlanId", planId);

		return "mealPlannerRecipeList";
	}
	
	@RequestMapping(path={"/{username}/mealPlannerDetail"}, method=RequestMethod.GET)
	public String displayMealPlannerDetail(Map<String, Object> model, HttpServletRequest request) {
		String recipeIdParse = request.getParameter("recipeId");
		String mealPlanIdParse = request.getParameter("mealPlanId");
		int recipeId = Integer.parseInt(recipeIdParse);
		int planId = Integer.parseInt(mealPlanIdParse);
		model.put("mealPlanner", recipeDao.getRecipeById(recipeId));
		model.put("planId", planId);
		model.put("directions", directionsDao.getDirectionsById(recipeId));
		model.put("ingredients", ingredientsDao.getIngredientsById(recipeId));
		return "mealPlannerDetail";
	}
	
	@RequestMapping(path={"{username}/newMealPlan"}, method=RequestMethod.POST)
	public String addNewPlan(@PathVariable String username, @RequestParam String planName) {
		userDao.createNewMealPlan(username, planName);
		return "newMealPlan";
	}
	
	@RequestMapping(path={"{username}/newMealPlanAddRecipe"}, method=RequestMethod.POST)
	public String addNewPlanNewRecipe(@PathVariable String username, @RequestParam String planName, @RequestParam int recipeId) {
		userDao.createNewMealPlan(username, planName);
		userDao.addRecipeToUsersPlan(username, recipeId, planDao.getLastPlanId());
		return "newMealPlan";
	}
	
	@RequestMapping(path={"{username}/shoppingList"}, method=RequestMethod.GET)
	public String displayMealPlanShoppingList(Map<String, Object> model, @PathVariable String username, @RequestParam int mealPlanId) {
		List<Ingredients> usersShoppingList = userDao.generateShoppingList(mealPlanId);
		model.put("shoppingList", usersShoppingList);
		return "shoppingList";
	}
	
	@RequestMapping(path={"//shoppingList"}, method=RequestMethod.GET)
	public String redirectShoppingList() {
		return "redirect:/login";
	}
	
	
	@RequestMapping(path={"/savedRecipes"}, method=RequestMethod.GET)
	public String redirectSavedRcipes() {	
		return "redirect:/login";
	}
	
	
	@RequestMapping(path={"{username}/savedRecipes"}, method=RequestMethod.GET)
	public String displaySavedRecipes(Map<String, Object> model, @PathVariable String username) {	
		model.put("savedRecipes", recipeDao.getRecipeByUsername(username));
		return "savedRecipes";
	}
	
	@RequestMapping(path={"//savedRecipes"}, method=RequestMethod.GET)
	public String redirectSavedRecipes() {
		return "redirect:/login";
		
	}
	
	@RequestMapping(path={"{username}/savedRecipeDetail"}, method=RequestMethod.GET)
	public String displaySavedRecipeDetail(HttpServletRequest request) {
		String recipeIdParse = request.getParameter("recipeId");
		int recipeId = Integer.parseInt(recipeIdParse);
		request.setAttribute("savedRecipes", recipeDao.getRecipeById(recipeId));
		request.setAttribute("directions", directionsDao.getDirectionsById(recipeId));
		request.setAttribute("ingredients", ingredientsDao.getIngredientsById(recipeId));
		request.setAttribute("user", userDao.getUsersByUsername(recipeDao.getRecipeById(recipeId).getUsername()));
		return "savedRecipeDetail";
	}
	
	@RequestMapping(path={"/{username}/recipeSave"}, method=RequestMethod.POST)
	public String recipeSave(@PathVariable String username, @RequestParam int recipeId) {
		try{
		userDao.addRecipeToUser(recipeId, username);
		return "recipeSave";
		} catch (DuplicateKeyException e){
			return "alreadyAdded";
		}
	}
	
	@RequestMapping(path={"//recipeSave"}, method=RequestMethod.POST)
	public String recipeSaveRedirect() {
		return "redirect:/login";
	}
	
	@RequestMapping(path={"/{username}/recipeDelete"}, method=RequestMethod.POST)
	public String recipeDelete(@PathVariable String username, @RequestParam int recipeId) {
		userDao.removeRecipeFromUser(username, recipeId);
		return "recipeDelete";
	}
	
	@RequestMapping(path={"/{username}/recipeRemove"}, method=RequestMethod.POST)
	public String recipeRemove(@PathVariable String username, @RequestParam int recipeId, @RequestParam int mealPlanId) {
		userDao.removeRecipeFromUsersPlans(recipeId, mealPlanId);
		return "recipeRemove";
	}

	@RequestMapping(path={"/healthy"}, method=RequestMethod.GET)
	public String displayHealthyRecipes(HttpServletRequest request) {
		List<Recipe> healthy = recipeDao.getAllHealthyRecipes();
		request.setAttribute("healthy", healthy);
		return "healthy";
	}
	
	@RequestMapping(path={"/paleo"}, method=RequestMethod.GET)
	public String displayPaleoRecipes(HttpServletRequest request) {
		List<Recipe> paleo = recipeDao.getAllPaleoRecipes();
		request.setAttribute("paleo", paleo);
		return "paleo";
	}
	@RequestMapping(path={"/vegetarian"}, method=RequestMethod.GET)
	public String displayVegetarianRecipes(HttpServletRequest request) {
		List<Recipe> vegetarian = recipeDao.getAllVegetarianRecipes();
		request.setAttribute("vegetarian", vegetarian);
		return "vegetarian";
	}
	
	@RequestMapping(path={"/{username}/healthy"}, method=RequestMethod.GET)
	public String displayHealthyRecipesUser(HttpServletRequest request) {
		List<Recipe> healthy = recipeDao.getAllHealthyRecipes();
		request.setAttribute("healthy", healthy);
		return "healthy";
	}
	
	@RequestMapping(path={"/{username}/paleo"}, method=RequestMethod.GET)
	public String displayPaleoRecipesUser(HttpServletRequest request) {
		List<Recipe> paleo = recipeDao.getAllPaleoRecipes();
		request.setAttribute("paleo", paleo);
		return "paleo";
	}
	@RequestMapping(path={"/{username}/vegetarian"}, method=RequestMethod.GET)
	public String displayVegetarianRecipesUser(HttpServletRequest request) {
		List<Recipe> vegetarian = recipeDao.getAllVegetarianRecipes();
		request.setAttribute("vegetarian", vegetarian);
		return "vegetarian";
	}
	
	@RequestMapping(path={"/{username}/modifyRecipe"}, method=RequestMethod.POST)
	public String displayModifyRecipe(Map<String, Object> model, @RequestParam int recipeId) {
		
		model.put("recipe", recipeDao.getRecipeById(recipeId));
		model.put("ingredients", ingredientsDao.getIngredientsById(recipeId));
		model.put("directions", directionsDao.getDirectionsById(recipeId));
		return "modifyRecipe";
	}
	
	@RequestMapping(path={"/{username}/updateRecipe"}, method=RequestMethod.POST)
	public String updateRecipe(@RequestParam int recipeId, @RequestParam String name, @RequestParam String description, @RequestParam int cookTime, @RequestParam int servings, @RequestParam String directions, @RequestParam String ingredients, HttpServletRequest request) {
		
		
		String healthyParam = request.getParameter("healthy");
		String paleoParam = request.getParameter("paleo");
		String vegetarianParam = request.getParameter("vegetarian");
		Boolean vegetarian;
		Boolean paleo;
		Boolean healthy;
		if (healthyParam == null) {
			healthy = false;
		} else {
			healthy = true;
		}
		if (paleoParam == null) {
			paleo = false;
		} else {
			paleo = true;
		}
		if (vegetarianParam == null) {
			vegetarian = false;
		} else {
			vegetarian = true;
		}
		
		recipeDao.modifyRecipe(recipeId, name, description, cookTime, servings, healthy, paleo, vegetarian);
		ingredientsDao.modifyIngredients(recipeId, ingredients);
		directionsDao.updateDirections(recipeId, directions);
		
		String redirect = "redirect:/recipeDetail?recipeId="+Integer.toString(recipeId);
		return redirect;
	}
	@RequestMapping(path={"/searchIngredient"}, method=RequestMethod.GET)
	public String searchRecipesByIngredient() {
	return "searchIngredient";
	}
	@RequestMapping(path={"/searchIngredient"}, method=RequestMethod.POST)
	public String searchRecipesByIngredient(HttpServletRequest request, @RequestParam String searchIngredient) {
		request.setAttribute("searchIngredient", searchIngredient);
		return "redirect:/searchIngredientResults";
	}

	@RequestMapping(path={"/searchIngredientResults"}, method=RequestMethod.GET)
	public String searchRecipesByIngredientResults(HttpServletRequest request, @RequestParam String searchIngredient) {
		List<Ingredients> searchIngredients = ingredientsDao.searchByIngredients(searchIngredient);
		List<Recipe> recipe = recipeDao.getAllRecipes();
		request.setAttribute("recipes", recipe);
		request.setAttribute("searchIngredients", searchIngredients);
		return "searchIngredientResults";
	}
}