package com.techelevator.plan;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcPlanDao implements PlanDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void JdbcUserDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Plan> getAllPlans() {
		List<Plan> allPlans = new ArrayList<>();
		String sqlSelectAllPlans = "SELECT * FROM meal_plan";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllPlans);
		while(results.next()) {
			Plan plan = new Plan();
			plan.setPlanId(results.getInt("plans_id"));
			plan.setName(results.getString("name"));
			allPlans.add(plan);
		}
		return allPlans;
	}

	@Override
	public List<Plan> getPlansByUsername(String username) {
		List<Plan> usernamePlans = new ArrayList<>();
		String sqlSelectPlansByUsername = "SELECT mp.plans_id, mp.name FROM meal_plan mp " +
										"INNER JOIN meal_plans_users mpu ON mp.plans_id = mpu.plans_id " +
										"INNER JOIN users u ON mpu.username = u.username " +
										"WHERE u.username = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectPlansByUsername, username);
		while(results.next()) {
			Plan plan = new Plan();
			plan.setPlanId(results.getInt("plans_id"));
			plan.setName(results.getString("name"));
			usernamePlans.add(plan);
		}
		return usernamePlans;
	}

	@Override
	public Plan getPlanByPlanId(int planId) {
		List<Plan> allPlans = getAllPlans();
		for (Plan plan : allPlans) {
			if(plan.getPlanId() == planId) {
				return plan;
			}
		}
		return null;
	}

	@Override
	public List<Plan> getPlansByName(String name) {
		List<Plan> allPlans = getAllPlans();
		List<Plan> namedPlans = new ArrayList<>();
		for (Plan plan : allPlans) {
			if(plan.getName().equals(name)) {
				namedPlans.add(plan);
			}
		}
		return namedPlans;
	}

	@Override
	public int getLastPlanId() {
		String sqlFindNewPlansId = "SELECT MAX(plans_id) FROM meal_plan";
		SqlRowSet sqlPlansId = jdbcTemplate.queryForRowSet(sqlFindNewPlansId);
		int plansId = 0;
		if(sqlPlansId.next()) {
			plansId = sqlPlansId.getInt(1);
		}
		return plansId;
	}
}
