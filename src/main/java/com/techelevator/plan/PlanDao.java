package com.techelevator.plan;

import java.util.List;

public interface PlanDao {
	public List<Plan> getAllPlans();
	public List<Plan> getPlansByUsername(String username);
	public Plan getPlanByPlanId(int planId);
	public List<Plan> getPlansByName(String name);
	public int getLastPlanId();
}
