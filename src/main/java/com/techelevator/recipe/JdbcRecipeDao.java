package com.techelevator.recipe;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.plan.Plan;

@Component
public class JdbcRecipeDao implements RecipeDao {
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcRecipeDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Recipe> getAllRecipes() {
		List<Recipe> allRecipes = new ArrayList<>();
		String sqlSelectAllRecipes = "SELECT * FROM recipe";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllRecipes);
		while(results.next()) {
			Recipe recipe = new Recipe();
			recipe.setRecipeId(results.getInt("recipe_id"));
			recipe.setName(results.getString("name"));
			recipe.setDescription(results.getString("description"));
			recipe.setCookTime(results.getInt("cook_time"));
			recipe.setServings(results.getInt("servings"));
			recipe.setHealthy(results.getBoolean("healthy"));
			recipe.setPaleo(results.getBoolean("paleo"));
			recipe.setVegatarian(results.getBoolean("vegetarian"));
			recipe.setUsername(results.getString("owner"));
			allRecipes.add(recipe);
		}
		return allRecipes;
	}

	@Override
	public Recipe getRecipeById(int recipeId) {
		List<Recipe> allRecipes = getAllRecipes();
		for (Recipe recipe : allRecipes) {
			if(recipe.getRecipeId() == recipeId) {
				return recipe;
			}
		}
		return null;
	}

	@Override
	public List<Recipe> getAllHealthyRecipes() {
		List<Recipe> allRecipes = getAllRecipes();
		List<Recipe> healthyRecipes = new ArrayList<>();
		for (Recipe recipe : allRecipes) {
			if(recipe.isHealthy()) {
				healthyRecipes.add(recipe);	
			}
		}
		return healthyRecipes;
	}

	@Override
	public List<Recipe> getAllPaleoRecipes() {
		List<Recipe> allRecipes = getAllRecipes();
		List<Recipe> paleoRecipes = new ArrayList<>();
		for (Recipe recipe : allRecipes) {
			if(recipe.isPaleo()) {
				paleoRecipes.add(recipe);
			}
		}
		return paleoRecipes;
	}

	@Override
	public List<Recipe> getAllVegetarianRecipes() {
		List<Recipe> allRecipes = getAllRecipes();
		List<Recipe> vegetarianRecipes = new ArrayList<>();
		for (Recipe recipe : allRecipes) {
			if(recipe.isVegatarian()) {
				vegetarianRecipes.add(recipe);
			}
		}
		return vegetarianRecipes;
	}

	@Override
	public void createNewRecipe(String name, String description, int cookTime, 
			int servings, boolean healthy, boolean paleo, boolean vegetarian, String username) {
		String sqlInsertRecipe = "INSERT INTO recipe(name, description, cook_time, servings, healthy, paleo, vegetarian, owner) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sqlInsertRecipe, name, description, cookTime, servings, healthy, paleo, vegetarian, username); 
	}
	
	@Override
	public void modifyRecipe(int recipeId, String name, String description, int cookTime, 
			int servings, boolean healthy, boolean paleo, boolean vegetarian) {
		String sqlInsertRecipe = "UPDATE recipe SET name=?, description=?, cook_time=?, servings=?, healthy=?, paleo=?, vegetarian=? WHERE recipe_id=?";
		jdbcTemplate.update(sqlInsertRecipe, name, description, cookTime, servings, healthy, paleo, vegetarian, recipeId); 
	}
	
	@Override
	public List<Recipe> getRecipeByUsername(String username) {
		List<Recipe> usernameRecipe = new ArrayList<>();
		String sqlSelectRecipeByUsername = "SELECT sr.recipe_id, r.name FROM saved_recipes sr INNER JOIN recipe r ON sr.recipe_id = r.recipe_id WHERE username = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectRecipeByUsername, username);
		while(results.next()) {
			Recipe recipe = new Recipe();
			recipe.setRecipeId(results.getInt("recipe_id"));
			recipe.setName(results.getString("name"));
			usernameRecipe.add(recipe);
		}
		return usernameRecipe;
	}

	@Override
	public List<Recipe> getRecipesByPlan(int planId) {
		List<Recipe> planRecipes = new ArrayList<>();
		String sqlSelectPlanRecipes = "SELECT name, r.recipe_id FROM meal_plans_recipes mpr INNER JOIN recipe r ON mpr.recipe_id = r.recipe_id WHERE mpr.plans_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectPlanRecipes, planId);
		while(results.next()) {
			Recipe recipe = new Recipe();
			recipe.setName(results.getString("name"));
			recipe.setRecipeId(results.getInt("recipe_id"));
			planRecipes.add(recipe);
		}
		return planRecipes;
	}


}
