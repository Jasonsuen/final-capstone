package com.techelevator.recipe;

public class Recipe {
	private int recipeId;
	private String name;
	private String description;
	private int cookTime;
	private int servings;
	private boolean healthy;
	private boolean paleo;
	private boolean vegetarian;
	private String username;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getCookTime() {
		return cookTime;
	}
	public void setCookTime(int cookTime) {
		this.cookTime = cookTime;
	}
	
	public int getServings() {
		return servings;
	}
	public void setServings(int servings) {
		this.servings = servings;
	}
	public boolean isHealthy() {
		return healthy;
	}
	public void setHealthy(boolean healthy) {
		this.healthy = healthy;
	}
	public boolean isPaleo() {
		return paleo;
	}
	public void setPaleo(boolean paleo) {
		this.paleo = paleo;
	}
	public boolean isVegatarian() {
		return vegetarian;
	}
	public void setVegatarian(boolean vegetarian) {
		this.vegetarian = vegetarian;
	}
}
