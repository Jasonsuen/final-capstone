package com.techelevator.recipe;

import java.util.List;

public interface RecipeDao {
	public List<Recipe> getAllRecipes();
	public Recipe getRecipeById(int recipeId);
	public List<Recipe> getAllHealthyRecipes();
	public List<Recipe> getAllPaleoRecipes();
	public List<Recipe> getAllVegetarianRecipes();
	public void createNewRecipe(String name, String description, int cookTime, 
								int servings, boolean healthy, boolean paleo, boolean vegetarian, String username);
	public List<Recipe> getRecipeByUsername(String username);
	public List<Recipe> getRecipesByPlan(int planId);
	public void modifyRecipe(int recipeId, String name, String description, int cookTime, 
								int servings, boolean healthy, boolean paleo, boolean vegetarian);
}
