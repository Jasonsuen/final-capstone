package com.techelevator.ingredients;

import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcIngredientsDao implements IngredientsDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcIngredientsDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Ingredients> getAllIngredients() {
		List<Ingredients> allIngredients = new ArrayList<>();
		String sqlSelectAllIngredients = "SELECT * FROM ingredients";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllIngredients);
		while(results.next()) {
			Ingredients ingredients = new Ingredients();
			ingredients.setIngredients(results.getString("ingredients"));
			ingredients.setIngredientsId(results.getInt("ingredients_id"));
			allIngredients.add(ingredients);
		}
		return allIngredients;
	}

	@Override
	public Ingredients getIngredientsById(int ingredientsId) {
		List<Ingredients> allIngredients = getAllIngredients();
		for (Ingredients ingredients : allIngredients) {
			if(ingredients.getIngredientsId() == ingredientsId) {
				return ingredients;
			}
		}
		return null;
	}

	@Override
	public void createNewIngredients(String ingredients) {
		String sqlInsertIngredients = "INSERT INTO ingredients(ingredients) VALUES (?)";
		jdbcTemplate.update(sqlInsertIngredients, ingredients);
	}

	@Override
	public void modifyIngredients(int recipeId, String ingredients) {
		String sqlUpdateIngredients = "UPDATE ingredients SET ingredients=? WHERE ingredients_id=?";
		jdbcTemplate.update(sqlUpdateIngredients, ingredients, recipeId);
		
	}
	
	@Override
	public List<Ingredients> searchByIngredients(String searchIngredient) {
		List<Ingredients> recipeWithIngredients = new ArrayList<>();
		String sqlUpdateIngredients = "SELECT recipe_id, ingredients, name  FROM recipe r INNER JOIN ingredients i ON i.ingredients_id = r.recipe_id WHERE i.ingredients LIKE ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlUpdateIngredients, "%" + searchIngredient.toLowerCase() + "%");
		while(results.next()) {
			Ingredients ingredients = new Ingredients();
			ingredients.setIngredients(results.getString("name"));
			ingredients.setIngredientsId(results.getInt("recipe_id"));
			recipeWithIngredients.add(ingredients);
		}
		return recipeWithIngredients;
	}
	
	
}
