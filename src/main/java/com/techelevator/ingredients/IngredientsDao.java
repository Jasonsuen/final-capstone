package com.techelevator.ingredients;

import java.util.List;


public interface IngredientsDao {
	public List<Ingredients> getAllIngredients();
	public Ingredients getIngredientsById(int ingredientsId);
	public void createNewIngredients(String ingredients);
	public void modifyIngredients(int recipeId, String ingredients);
	List<Ingredients> searchByIngredients(String ingredientsId);
}
