package com.techelevator.user;

import java.util.List;

import com.techelevator.ingredients.Ingredients;

public interface UserDao {

	public List<User> getAllUsers();
	public User getUsersByUsername(String username);
	public void createNewUser(String username, String password, String firstName, String lastName);
	public void changePassword(String username, String password);
	public boolean searchForUsernameAndPassword(String userName, String password);
	public void addRecipeToUser(int recipeId, String username);
	public void removeRecipeFromUser(String username, int recipeId);
	public void createNewMealPlan(String username, String planName);
	public void addRecipeToUsersPlan(String username, int recipeId, int mealPlanId);
	public void removeRecipeFromUsersPlans(int recipeId, int mealPlanId);
	public void removeAllRecipesFromPlan(int mealPlanId);
	public void removeEntirePlan(String username, int mealPlanId);
	public List<Ingredients> generateShoppingList(int mealPlanId);
	public String totalSubmitsSavedToUser(String username);
}