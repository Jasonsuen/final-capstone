package com.techelevator.user;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.ingredients.Ingredients;
import com.techelevator.security.PasswordHasher;

@Component
public class JdbcUserDao implements UserDao {
	
	private JdbcTemplate jdbcTemplate;
	private PasswordHasher passwordHasher;

	@Autowired
	public JdbcUserDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.passwordHasher = new PasswordHasher();
	}

	@Override
	public List<User> getAllUsers() {
		List<User> allUsers = new ArrayList<>();
		String sqlSelectAllUsers = "SELECT * FROM users";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllUsers);
		while(results.next()) {
			User user = new User();
			user.setUsername(results.getString("username"));
			user.setPassword(results.getString("password"));
			user.setFirstName(results.getString("first_name"));
			user.setLastName(results.getString("last_name"));
			allUsers.add(user);
		}
		return allUsers;
	}

	@Override
	public User getUsersByUsername(String username) {
		List<User> allUsers = getAllUsers();
		for (User user: allUsers) {
			if (user.getUsername().equals(username)) {
				return user;
			}
		}
		return null;
	}

	@Override
	public void createNewUser(String username, String password, String firstName, String lastName) {
		byte[] salt = passwordHasher.generateRandomSalt();
		String hashedPassword = passwordHasher.computeHash(password, salt);
		String saltString = new String(Base64.encode(salt));
		String sqlInsertUser = "INSERT INTO users(username, password, first_name, last_name, salt) VALUES (?, ?, ?, ?, ?)";
		jdbcTemplate.update(sqlInsertUser, username, hashedPassword, firstName, lastName, saltString);
	}

	@Override
	public void changePassword(String username, String password) {
		byte[] salt = passwordHasher.generateRandomSalt();
		String hashedPassword = passwordHasher.computeHash(password, salt);
		String saltString = new String(Base64.encode(salt));
		System.out.println("Username: " + username);
		System.out.println("Password: " + password);
		int rows = jdbcTemplate.update("UPDATE users SET password = ?, salt = ? WHERE UPPER(username) = ?", hashedPassword, saltString, username.toUpperCase());
		System.out.println("Rows: " + rows);
	}

	@Override
	public boolean searchForUsernameAndPassword(String userName, String password) {
		String sqlSearchForUser = "SELECT * FROM users WHERE UPPER(username) = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchForUser, userName.toUpperCase());
		if(results.next()) {
			String storedSalt = results.getString("salt");
			String storedPassword = results.getString("password");
			String hashedPassword = passwordHasher.computeHash(password, Base64.decode(storedSalt));

			return storedPassword.equals(hashedPassword);
		} else {
			return false;
		}
	}

	@Override
	public void addRecipeToUser(int recipeId, String username) {
		String sqlAddRecipeToUser = "INSERT INTO saved_recipes(recipe_id, username) VALUES (?, ?)";
		jdbcTemplate.update(sqlAddRecipeToUser, recipeId, username);
	}

	@Override
	public void removeRecipeFromUser(String username, int recipeId) {
		String sqlRemoveRecipeFromUser = "DELETE FROM saved_recipes WHERE username = ? AND recipe_id = ?";
		jdbcTemplate.update(sqlRemoveRecipeFromUser, username, recipeId);
	}

	@Override
	public void addRecipeToUsersPlan(String username, int recipeId, int mealPlanId) {
		String sqlAddRecipeToUsersPlan = "INSERT INTO meal_plans_recipes(recipe_id, plans_id) VALUES (?, ?)";
		jdbcTemplate.update(sqlAddRecipeToUsersPlan, recipeId, mealPlanId);	
	}

	@Override
	public void removeRecipeFromUsersPlans(int recipeId, int mealPlanId) {
		String sqlRemoveRecipeFromUsersPlans2 = "DELETE FROM meal_plans_recipes WHERE plans_id = ? AND recipe_id = ?";
		jdbcTemplate.update(sqlRemoveRecipeFromUsersPlans2, mealPlanId, recipeId);
	}

	@Override
	public List<Ingredients> generateShoppingList(int mealPlanId) {
		List<Ingredients> usersShoppingList = new ArrayList<>();
		String sqlGenerateShoppingList = "SELECT * FROM ingredients i " +
										"INNER JOIN recipe r ON i.ingredients_id = r.recipe_id " +
										"INNER JOIN meal_plans_recipes mpr ON r.recipe_id = mpr.recipe_id " +
										"INNER JOIN meal_plan mp ON mpr.plans_id = mp.plans_id " +
										"INNER JOIN meal_plans_users mpu ON mp.plans_id = mpu.plans_id " +
										"WHERE mpu.plans_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGenerateShoppingList, mealPlanId);
		while(results.next()) {
			Ingredients ingredients = new Ingredients();
			ingredients.setIngredientsId(results.getInt("ingredients_id"));
			ingredients.setIngredients(results.getString("ingredients"));
			usersShoppingList.add(ingredients);
		}
		
		return usersShoppingList;
	}

	@Override
	public void removeAllRecipesFromPlan(int mealPlanId) {
		String sqlRemoveAllPlans = "DELETE FROM meal_plans_recipes WHERE plans_id = ?";
		jdbcTemplate.update(sqlRemoveAllPlans, mealPlanId);
	}

	@Override
	public void createNewMealPlan(String username, String planName) {
		String sqlCreateNewPlan = "INSERT INTO meal_plan(name) VALUES (?)";
		String sqlCreateNewPlan2 = "INSERT INTO meal_plans_users(plans_id, username) VALUES (?, ?)";
		jdbcTemplate.update(sqlCreateNewPlan, planName);
		String sqlFindNewPlansId = "SELECT MAX(plans_id) FROM meal_plan";
		SqlRowSet sqlPlansId = jdbcTemplate.queryForRowSet(sqlFindNewPlansId);
		int plansId = 0;
		if(sqlPlansId.next()) {
			plansId = sqlPlansId.getInt(1);
		}
		jdbcTemplate.update(sqlCreateNewPlan2, plansId, username);
	}

	@Override
	public void removeEntirePlan(String username, int mealPlanId) {
		String sqlRemoveRecipeFromUsersPlans = "DELETE FROM meal_plans_users WHERE username = ? AND plans_id = ?";
		String sqlRemoveRecipeFromUsersPlans2 = "DELETE FROM meal_plans_recipes WHERE plans_id = ?";
		String sqlRemovePlan = "DELETE FROM meal_plan WHERE plans_id = ?";
		jdbcTemplate.update(sqlRemoveRecipeFromUsersPlans, username, mealPlanId);
		jdbcTemplate.update(sqlRemoveRecipeFromUsersPlans2, mealPlanId);
		jdbcTemplate.update(sqlRemovePlan, mealPlanId);
	}
	
	@Override
	public String totalSubmitsSavedToUser(String username) {
		String sqltotalSubmitsSavedToUser = "SELECT COUNT(recipe_id) FROM recipe WHERE owner = ?";                                                              ;
		SqlRowSet sqlcount = jdbcTemplate.queryForRowSet(sqltotalSubmitsSavedToUser, username);
		String count = null;
		if(sqlcount.next()){
			count = sqlcount.getString(1);
		}
		return count;
	}
}