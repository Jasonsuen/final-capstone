<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
	<h1 class="title">Select Your Meal Plan</h1>

	<ul class="products">
		<c:forEach var="mealPlan" items="${mealPlans}">
			<li><a href="addRecipe?recipeId=${recipeId}&planId=${mealPlan.planId}">${mealPlan.name}</a></li>
		</c:forEach>
	</ul>
	<form action="/capstone/${currentUser}/newMealPlanAddRecipe${newMealPlan}?recipeId=${recipeId}" method="POST">
		<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
		<input type="text" name="planName" placeholder="Name">
		<button type="submit" id="newMealPlan">Add new meal plan</button>
	</form>

</div>


<%@ include file="footer.jsp"%>