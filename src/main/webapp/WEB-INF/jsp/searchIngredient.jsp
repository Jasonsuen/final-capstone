<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>
<div class="content">
	<h2 class="title">Find a recipe by your favorite ingredients.</h2>
			<c:url var="formHref" value="/searchIngredientResults"/>
			<form action="${formHref}" method="GET">
			<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
				<div class="formGroup">
					<label for="searchIngredient">Ingredient</label>
					<input type="text" name="searchIngredient" id="searchIngredient" />
				</div>
				<br>
				<div class="formGroup">
					<input type="submit" value="Search Recipes" />
				</div>
			</form>
			<br>
			<br>
<%@ include file="footer.jsp"%>

