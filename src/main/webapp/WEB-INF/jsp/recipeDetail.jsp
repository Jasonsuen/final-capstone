<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
	<c:url var="recipeImage" value="/img/${recipe.name}.jpg"/>
	<c:url var="stockImage" value="/img/stockPhoto.jpg"/>
	<div class="detailsHead">
	<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="imageDetail"/>
	<div id="background">
	<h1 class= "recipeTitle"><span>${recipe.name}</span></h1>
	<h4>Recipe submitted by: ${user.firstName} ${user.lastName}</h4>
	</div>
	</div>
	<p>${recipe.description }<p>
	<p>Cook Time: ${recipe.cookTime} minutes<p>
	<p>Servings: ${recipe.servings}<p>
	<p>Ingredients: <br>
	<c:forTokens var="token" items="${ingredients.ingredients}"
                     delims="|">
            <c:out value="${token}"/> <br>
        </c:forTokens>
	</p>
	
	<p>Directions:<br>
	<p>
	<c:forTokens var="token" items="${directions.directions}"
                     delims="|">
            <c:out value="${token}"/> </p><p>
        </c:forTokens> 
	</p>
	</p>
	
	<form action="/capstone/${currentUser}/recipeSave?recipeId=${recipe.recipeId}" method="POST">
		<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
		<input type="hidden" name="recipeId" value="${recipeId}">
	    <button type="submit" id="recipeSave" >Save this recipe</button>
	</form>
	<form action="/capstone/${currentUser}/planPicker?recipeId=${recipe.recipeId}" method="POST">
		<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
		<input type="hidden" name="recipeId" value="${recipeId}">
	    <button type="submit" id="addRecipeToPlan" >Add recipe to a plan</button>
	</form>
	
	<form action="/capstone/${currentUser}/modifyRecipe?recipeId=${recipe.recipeId}" method="POST">
		<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
		<input type="hidden" name="recipeId" value="${recipeId}">
		<c:if test="${recipe.username == currentUser}">
		<button type="submit" id="modifyRecipe" >Modify this recipe</button>
		</c:if>
	</form> 
	<br>
	<br>

<%@ include file="footer.jsp"%>