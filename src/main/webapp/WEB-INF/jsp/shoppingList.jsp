<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
			<!---- for each here for home page ---->
			<h1 class ="title"> Shopping List</h1>
			<ul>
			<c:forEach var="shoppingList" items="${shoppingList}">
			<c:forTokens var="token" items="${shoppingList.ingredients}"
                     delims="|">
            <c:out value="${token}"/> <br>
        	</c:forTokens>
			</c:forEach>
			</ul>
			<!-- Ajax loaded content here -->

<%@ include file="footer.jsp"%>