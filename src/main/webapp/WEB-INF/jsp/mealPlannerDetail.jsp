<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
	<c:url var="recipeImage" value="/img/${mealPlanner.name}.jpg"/>
	<c:url var="stockImage" value="/img/stockPhoto.jpg"/>
	<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="imageDetail"/>
	<h1>${mealPlanner.name}</h1><h4>Recipe submitted by: ${mealPlanner.username}</h4>
	<p>${mealPlanner.description }<p>
	<p>Cook Time: ${mealPlanner.cookTime} minutes<p>
	<p>Servings: ${mealPlanner.servings}<p>
	<p>Ingredients: <br>
	<c:forTokens var="token" items="${ingredients.ingredients}"
                     delims="|">
            <c:out value="${token}"/> <br>
        </c:forTokens>
	</p>
	
	<p>Directions:<br>
	<p>
	<c:forTokens var="token" items="${directions.directions}"
                     delims="|">
            <c:out value="${token}"/> </p><p>
        </c:forTokens> 
	</p>
	</p>
	
	<form action="/capstone/${currentUser}/recipeRemove?recipeId=${mealPlanner.recipeId}&mealPlanId=${planId}" method="POST">
		<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
		<input type="hidden" name="recipeId" value="${recipeId}">
	    <button type="submit" id="recipeRemove" >Remove this recipe</button>
	</form>

<%@ include file="footer.jsp"%>