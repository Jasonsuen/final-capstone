
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp" />
		<div class="content">
			<!---- for each here for home page ---->
			<ul class= "info"><c:forEach var="recipe" items="${recipeList}">
			
			<li><a href="recipeDetail?recipeId=${recipe.id}"><h3>${recipe.name}</h3></a></li>
			</c:forEach>
			
			</ul>
			<!-- Ajax loaded content here -->
		</div>
		
<c:import url="/WEB-INF/jsp/footer.jsp" />
