<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
	<c:url var="recipeImage" value="/img/${savedRecipes.name}.jpg"/>
	<c:url var="stockImage" value="/img/stockPhoto.jpg"/>
	<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="imageDetail"/>
	<h1>${savedRecipes.name}</h1><h4>Recipe submitted by: ${user.firstName} ${user.lastName}</h4>
	<p>${savedRecipes.description }<p>
	<p>Cook Time: ${savedRecipes.cookTime} minutes<p>
	<p>Servings: ${savedRecipes.servings}<p>
	<p>Ingredients: <br>
	<c:forTokens var="token" items="${ingredients.ingredients}"
                     delims="|">
            <c:out value="${token}"/> <br>
        </c:forTokens>
	</p>
	
	<p>Directions:<br>
	<p>
	<c:forTokens var="token" items="${directions.directions}"
                     delims="|">
            <c:out value="${token}"/> </p><p>
        </c:forTokens> 
	</p>
	</p>
	<form action="/capstone/${currentUser}/recipeDelete?recipeId=${savedRecipes.recipeId}" method="POST">
		<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
		<input type="hidden" name="recipeId" value="${recipeId}">
	    <button type="submit" id="recipeDelete" >Delete this recipe</button>
	</form>
	<br>
	<br>

<%@ include file="footer.jsp"%>