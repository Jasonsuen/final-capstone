<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>

<div class= "content">
<form id="signUpForm" method="POST" action="${formAction}">
<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="form-group">
				<label for="firstName">First Name: </label>
				<input type="text" id="firstName" name="firstName" placeHolder="First Name" class="form-control" />
			</div>
				<div class="form-group">
				<label for="lastName">Last Name: </label>
				<input type="text" id="lastName" name="lastName" placeHolder="Last Name" class="form-control" />
			</div>
			<div class="form-group">
				<label for="username">User Name: </label>
				<input type="text" id="username" name="username" onblur="caps(this.id)" placeHolder="Email" class="form-control" />
			</div>
			<div class="form-group">
				<label for="password">Password: </label>
				<input type="password" id="password" name="password" placeHolder="Password" class="form-control" />
			</div>
			<div class="form-group">
				<label for="confirmPassword">Confirm Password: </label>
				<input type="password" id="confirmPassword" name="confirmPassword" placeHolder="Re-Type Password" class="form-control" />	
			</div>
			<button type="submit" class="btn btn-default">Create User</button>
		</div>
		<div class="col-sm-4"></div>
	</div>
</form>
</div>


<script type="text/javascript">
	$("#signUpForm").validate({
		
		rules : {
			username : {
				required: true,
				email: true
	        },
	        firstName: {
                required: true,
                minlength: 2
			},
            lastName: {
                required: true,
                minlength: 2
			},
			password : {
				required : true,
				minlength: 8,
				strongpassword: true
			},
			confirmPassword : {
				required : true,		
				equalTo : "#password"  
			}
		},
		messages : {			
			username : {
				required: "Cannot be empty",
				email: "Not a valid email address"
	        },
	        firstName: {
	        	required: "Cannot be empty",
                minlength: "Must be over 2 characters"
			},
            lastName: {
            	required: "Cannot be empty",
            	minlength: "Must be over 2 characters"
			},
			password : {
				required: "Cannot be empty",
				minlength: "Must be over 8 characters",
				strongpassword: "Not a strong password"
			},
			confirmPassword : {
				required: "Cannot be empty",
				equalTo : "Passwords don't match"  
			}
		},
		errorClass: "error",
        validClass: "valid"
	});
</script>
<script>
	function caps(id){
    document.getElementById(id).value = document.getElementById(id).value.toLowerCase();
}
</script>



<%@ include file="footer.jsp"%>