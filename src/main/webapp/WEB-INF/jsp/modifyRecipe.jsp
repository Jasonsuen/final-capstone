<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="header.jsp"%>

<div class= "content">
	<form name="form" action="/capstone/${currentUser}/updateRecipe?recipeId=${recipe.recipeId}" method="POST">
		<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/><br>
		<div class="formGroup">
			<label for="name">Name</label>
			<input type="text" id="name" name="name" value="${recipe.name}"><br>
		</div>
		<br>
		<div class="formGroup">
			<label for="description">Description</label>
			<textArea name="description" id="description">${recipe.description}</textArea><br>
		</div>
		<br>
		<div class="formGroup">
			<label for="cookTime">Cook Time (in minutes)</label>
			<input type="number" name="cookTime" value="${recipe.cookTime}"><br>
		</div>
		<br>
		<div class="formGroup">
			<label for="servings">Servings</label>
			<input type="number" name="servings" value="${recipe.servings}"><br>
		</div>
		<br>
		<div class="formGroup">
			<label for="ingredients">Ingredients</label>
			<c:set var="items" value="${fn:join(fn:split(ingredients.ingredients, '|'), '&#13;&#10;')}"/>
			<textarea name="ingredients" id="ingredients">${items}</textarea>
			<label for="ingredients">(seperate by new lines)</label>
		</div>
		<br>
		<div class="formGroup">
			<label for="directions">Directions</label>
			<c:set var="items2" value="${fn:join(fn:split(directions.directions, '|'), '&#13;&#10;')}"/>
			<textarea name="directions" id="directions">${items2}</textarea>
			<label for="directions">(seperate by new lines)</label>
		</div>
		<br>
		<div class="formGroup">
			<p>Is the recipe any of the following?</p>
			<input type="checkbox" name="healthy" value="healthy">Healthy<br>
			<input type="checkbox" name="paleo" value="paleo">Paleo<br>
			<input type="checkbox" name="vegetarian" value="vegetarian">Vegetarian 
  		</div>
		<br>
		<button type="submit" id="modifyRecipe" onclick="replace()">Improve Recipe</button>
	</form>
	<br>


<script>
document.getElementById('name').style.width="300px";
document.getElementById('ingredients').style.width="300px";
document.getElementById('ingredients').style.height="100px";
document.getElementById('directions').style.width="300px";
document.getElementById('directions').style.height="100px";
document.getElementById('description').style.width="300px";
document.getElementById('description').style.height="100px";

function replace() {
	replacer(document.form.ingredients);
	replacer(document.form.directions);
}

function replacer(where) {
	var origString = where.value;
	var inChar = "\n";
	var outChar = "|"
	var newString = origString.split(inChar);
	newString = newString.join(outChar);
	where.value = newString;
}
</script>

<%@ include file="footer.jsp"%>