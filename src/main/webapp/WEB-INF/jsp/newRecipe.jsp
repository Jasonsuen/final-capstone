<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>
<div class="content">
	<h2 class="title">Thanks for giving us new ideas!</h2>
			<form action="/capstone/${currentUser}/newRecipe" method="POST">
			<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
				<div class="formGroup">
					<label for="name">Name</label>
					<input type="text" name="name" id="name" />
				</div>
				<br>
				<div class="formGroup">
					<label for="description">Description</label>
					<textarea name="description" id="description" ></textarea>
				</div>
				<br>
				<div class="formGroup">
					<label for="cookTime">Cook time in minutes</label>
					<input type="number" name="cookTime" id="cookTime" />
				</div>
				<br>
				<div class="formGroup">
					<label for="servings">Servings</label>
					<input type="number" name="servings" id="servings" />
				</div>
				<br>
				<div class="formGroup">
					<label for="directions">Directions</label>
					<textarea name="directions" id="directions" ></textarea>
					<label for="directions">(seperate by new lines)</label>
				</div>
				<br>
				<div class="formGroup">
					<label for="ingredients">Ingredients</label>
					<textarea name="ingredients" id="ingredients" ></textarea>
					<label for="ingredients">(seperate by new lines)</label>
				</div>
				<br>
				<div class="formGroup">
					<p>Is the recipe any of the following?</p>
					<input type="checkbox" name="healthy" value="healthy">Healthy<br>
					<input type="checkbox" name="paleo" value="paleo">Paleo<br>
  					<input type="checkbox" name="vegetarian" value="vegetarian">Vegetarian 
  				</div>
  				<br>
				<div class="formGroup">
					<input type="submit" value="Add Recipe" />
				</div>
			</form>
			<br>
			<br>
			
<script>
document.getElementById('name').style.width="300px";
document.getElementById('ingredients').style.width="300px";
document.getElementById('ingredients').style.height="100px";
document.getElementById('directions').style.width="300px";
document.getElementById('directions').style.height="100px";
document.getElementById('description').style.width="300px";
document.getElementById('description').style.height="100px";
</script>

<%@ include file="footer.jsp"%>