<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
			<!---- for each here for home page ---->
			
			<h1 class="title">Saved Recipes</h1>
			<ul class="products">
			<c:forEach var="savedRecipes" items="${savedRecipes}">
			<c:url var="recipeImage" value="/img/${savedRecipes.name}.jpg"/>
			<c:url var="stockImage" value="/img/stockPhoto.jpg"/>
			<li class="product">
				<a href="savedRecipeDetail?recipeId=${savedRecipes.recipeId}" >
				<h2 class="productTitle"><span>${savedRecipes.name}</span></h2>
				<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="productImage"/>
			</a>
			</li>
			
			
			</c:forEach>
			</ul>
			<!-- Ajax loaded content here -->
			



<%@ include file="footer.jsp"%>