<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

	<div class="content">

			<ul class="products">
			<c:forEach var="healthy" items="${healthy}">
			<c:url var="recipeImage" value="/img/${healthy.name}.jpg"/>
		 	<li class="product">

   				<a href="recipeDetail?recipeId=${healthy.recipeId}">   
    
    			<h2 class="productTitle">${healthy.name}</h2>
       			<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="productImage"/>
			     </a>
			    </li>

    
			</c:forEach>
			</ul>		

<%@ include file="footer.jsp"%>