<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>
			
			<div class="content" >

<c:url var="formAction" value="/users/${currentUser}/changePassword" />

<div class="row">
	<div class="col-md-5">
		<form action="${formAction}" method="POST" id="changePasswordForm">
			<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
		
			<div class="form-group">
				<label for="password">New Password: </label>
				<input type="password" id="password" name="password" class="form-control" />	
			</div>
			<div class="form-group">
				<label for="confirmPassword">Confirm Password: </label>
				<input type="password" id="confirmPassword" name="confirmPassword" class="form-control" />	
			</div>
			<button type="submit" class="btn btn-default">Change Password</button>
		</form>
	</div>

	<div class="col-md-9"></div>
	</div>
</div>
<%@ include file="footer.jsp"%>