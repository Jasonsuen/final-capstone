<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

	<div class="content">
			<!---- for each here for home page ---->


<h1 class="title">Check out our Vegetarian meals!</h1>
			<ul class="products">
			<c:forEach var="vegetarian" items="${vegetarian}">
			<c:url var="recipeImage" value="/img/${vegetarian.name}.jpg"/>

		 	
			<li class="product">

		 	<c:url var="stockImage" value="/img/stockPhoto.jpg"/>
			<%-- <li class="product"style="background-image:url('${recipeImage}');  -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;"> --%>

			   <a href="recipeDetail?recipeId=${vegetarian.recipeId}">
			   <h2 class="productTitle">${vegetarian.name}</h2>
   
       			<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="productImage"/>
     </a>
    </li>
    
			</c:forEach>
			</ul>
			<!-- Ajax loaded content here -->
		

<%@ include file="footer.jsp"%>