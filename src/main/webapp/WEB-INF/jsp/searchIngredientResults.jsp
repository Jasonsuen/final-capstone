<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
<h1 class="title">Search Results:</h1>
			<ul class="products">
			<c:forEach var="searchIngredients" items="${searchIngredients}"> 
			<li class="product">
			<c:url var="recipeImage" value="/img/${searchIngredients.ingredients}.jpg"/>
			<c:url var="stockImage" value="/img/stockPhoto.jpg"/>
			<a href="recipeDetail?recipeId=${searchIngredients.ingredientsId}" id="${recipe.name}">
			<h2 class="productTitle"><span>${searchIngredients.ingredients}</span></h2>
			<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="productImage"/>

			</a>
   			 </li>
			</c:forEach>
			</ul>
<%@ include file="footer.jsp"%>