<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>


<div class="content">
<div class="about">
	<h1 >Plan To Eat Better.</h1>
	<p>Healthy eating is about recognizing the role that food plays in
		supporting or undermining your emotional and physical health. It's
		also about exploring your relationship to food. When it comes to
		making healthy food choices, what works best for one person may not
		always be the best choice for another, especially at a time when what
		constitutes healthy eating is being hotly disputed. This meal planner
		is taylormade to supporting your healthy eating habits.</p>
</div>


<%@ include file="footer.jsp"%>