<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
			<!---- for each here for home page ---->
			<h1 class="title">Meal Plans</h1>
			<ul>
			<c:forEach var="mealPlan" items="${mealPlans}">
			<c:url var="recipeImage" value="/img/${mealPlan.name}.jpg"/>
			<li><a href="mealPlannerRecipeList?planId=${mealPlan.planId}">${mealPlan.name}</a></li>
			</c:forEach>
			</ul>
			<!-- Ajax loaded content here -->
			<form action="/capstone/${currentUser}/newMealPlan${newMealPlan}" method="POST">
				<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
				<input type="text" name="planName" placeholder="Name">
				<button type="submit" id="newMealPlan">Add new meal plan</button>
			</form>
</div>
<form action="/capstone/${currentUser}/shoppingList?planId=${mealPlanId}" method="POST">
<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
<input type="hidden" name="recipeId" value="${mealPlanId}">
<button type="submit" id="shoppingList" >Shopping List</button>
</form>

<%@ include file="footer.jsp"%>