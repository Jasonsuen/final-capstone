<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>



			
<div class="content" >
	<c:url var="formAction" value="/login" />
	<form id="logInForm" method="POST" action="${formAction}">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<input type="hidden" name="destination" value="${param.destination}" />
			<div class="form-group">
				<label for="username" >User Name: </label>
				<input type="text" id="username" name="username" onblur="caps(this.id)" placeHolder="User Name" class="form-control" />

			</div>
			<div class="form-group">
				<label for="password">Password: </label>
				<input type="password" id="password" name="password" placeHolder="Password" class="form-control" />
			</div>
			<button type="submit">Login</button>
			
		</form>
			 
			</div>
<script type="text/javascript">	
		$("#logInForm").validate({

			
			rules : {
				username : {
					required : true,
					email: true
				},
				password : {

					required : true,
					minlength: 8
				}
			},
			messages:{
				username:{
					required:"Username can't be left blank",
					email:"Must be a valid email address"
				},
				password:{
					required:"Password cannot be empty",

					required : true,
				}
			},
			messages : {	
				username : {
					required : "Please enter a username",
					email: "Please enter a username that is a valid email address"
				},
				Password : {
					required: "Please enter a password"

				}
				
			},

			
			

			errorClass : "error",
			validClass: "valid"

		});
		
</script>
			
		

<script>
	function caps(id){
    document.getElementById(id).value = document.getElementById(id).value.toLowerCase();
}
</script>



<%@ include file="footer.jsp"%>