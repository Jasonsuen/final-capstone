<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
	
	<h1>You have deleted your meal plan.</h1>
	<p>Let's build another one!</p>

</div>

<%@ include file="footer.jsp"%>