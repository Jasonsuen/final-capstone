<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Meal Planner</title>
	<meta name="description" content="Meal Planner" />
	
	
	
	<!-- food icons -->
	<c:url var="iconsCssHref" value="/css/organicfoodicons.css" />
	<link rel="stylesheet" type="text/css" href="${iconsCssHref}" />
	
	<c:url var="demoCssHref" value="/css/demo.css" /><!-- demo styles -->
	<link rel="stylesheet" type="text/css" href="${demoCssHref}" />
	<!-- menu styles -->
	<c:url var="componentCssHref" value="/css/component.css" />
	<link rel="stylesheet" type="text/css" href="${componentCssHref}" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<c:url var="modernizerHref" value="/js/modernizr-custom.js"></c:url>
	<script src="${modernizerHref}"></script>
	<script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
    <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.js "></script>
    <script src="https://cdn.jsdelivr.net/jquery.timeago/1.4.1/jquery.timeago.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		/* $(document).ready(function() {
			$("time.timeago").timeago();
			
			$("#logoutLink").click(function(event){
				$("#logoutForm").submit();
			});
			
	
		}); */
	</script>
</head>

<body>
	<!-- Main container -->
	<div class="container">
		<!-- Blueprint header -->
		<header class="bp-header cf">
			<div class="dummy-logo" >
				<c:url var="recipeImage" value="/img/Prep Logo.png"/>
				<a href="/capstone/${currentUser}"><img class="logo" src="${recipeImage}"></a>
				<div class="dummy-icon foodicon foodicon--coconut"></div>
				<h2 class="dummy-heading"><a href = "homePage">Meal Planner</a></h2>
			</div>
			<div class="bp-header__main">
				<!-- <span class="bp-header__present">Login <span class="bp-tooltip bp-icon bp-icon--about" data-content="The Blueprints are a collection of basic and minimal website concepts, components, plugins and layouts with minimal style for easy adaption and usage, or simply for inspiration."></span></span> -->
				
				
				<c:choose>
				<c:when test="${empty currentUser}">
				<c:url var="loginHref" value="/login" />
				<h1 class="bp-header__title"> <a href="${loginHref}" id ="Login"> Login </a> || 
				<c:url var="signUpHref" value="/users/new" />
				<a href = "signUp" id="Sign Up">Sign Up</a></h1>
				</c:when>
						<c:otherwise>
							<c:url var="logoutAction" value="/logout" />
						
							<h1  class="bp-header__title"><a id="logoutLink" href="${logoutAction}">Log Out</a></h1>
						</c:otherwise>
					</c:choose>
				
			
			<c:if test="${not empty currentUser}">
			<p id="currentUser">Current User: ${currentUser}</p>
			</c:if>		
		
		
				<nav class="bp-nav">
				<c:url var="mealPlannerHref" value="/${currentUser}/mealPlanner"/>
				<c:url var="savedRecipesHref" value="/${currentUser}/savedRecipes"/>
				<c:url var="profileHref" value="/${currentUser}/userDashboard"/>
					<a class="bp-nav__item "  href="${mealPlannerHref}" data-info="View Meals" id ="ViewMeals"><span>View Meals</span></a>
					<!--a class="bp-nav__item bp-icon bp-icon--next" href="" data-info="next Blueprint"><span>Next Blueprint</span></a-->
					<a class="bp-nav__item " href="${savedRecipesHref}" data-info="View Saved Recipes" id ="ViewSavedRecipes"><span>View Saved Recipes</span></a>
					<a class="bp-nav__item " href="${profileHref}" data-info="View Profile" id="ViewProfile"><span>Go to the archive</span></a>
				</nav>
			</div>
		</header>
		<button class="action action--open" aria-label="Open Menu"><span class="icon icon--menu"></span></button>
		<nav id="ml-menu" class="menu">
			<button class="action action--close" aria-label="Close Menu"><span class="icon icon--cross"></span></button>
			<div class="menu__wrap">
				<ul data-menu="main" class="menu__level">
					<li class="menu__item"><a class="menu__link" data-submenu="submenu-1" href="#">Meal Type</a></li> 
					<li class="menu__item"><a class="menu__link"  href="aboutUs">About Us</a></li>
					<li class="menu__item" ><a class="menu__link"href="/capstone/${currentUser}/newRecipe">Add a Recipe</a></li>
					<li class="menu__item" ><a class="menu__link"href="/capstone/searchIngredient">Search by ingredient</a></li>
					<!-- <li class="menu__item"><a class="menu__link"  href="/mealPlanner">Meal Planner</a></li> -->
					<!-- <li class="menu__item"><a class="menu__link" data-submenu="submenu-3" href="#">View Meals</a></li>
					<li class="menu__item"><a class="menu__link" data-submenu="submenu-4" href="#">View Shopping</a></li> -->
				</ul>
				<!-- Submenu 1 -->
				<ul data-menu="submenu-1" class="menu__level">
					<li><a class="menu__link" href="paleo">Paleo</a></li>
					<li><a class="menu__link" href="healthy">Healthy</a></li>
					
					<li><!-- class="menu__item" --><a class="menu__link" href="vegetarian">Vegetarian</a></li>
					
					
				</ul>
				
			</div>
		</nav>
			