<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

	<div class="content">
			

<h1 class="title">Check out our paleo meals!</h1>

			<ul class="products">
			<c:forEach var="paleo" items="${paleo}">
			<c:url var="recipeImage" value="/img/${paleo.name}.jpg"/>

		 	
			<li class="product">

			<c:url var="stockImage" value="/img/stockPhoto.jpg"/>
			<%-- <li class="product"style="background-image:url('${recipeImage}');  -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;"> --%>
   				<a href="recipeDetail?recipeId=${paleo.recipeId}">
<%--     <img class="product" src="${recipeImage}"> --%>

    
    			<h2 class="productTitle">${paleo.name}</h2>
    			<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="productImage"/>
     			</a>
   				</li>
    
			</c:forEach>
			</ul>
			<!-- Ajax loaded content here -->

		

<%@ include file="footer.jsp"%>