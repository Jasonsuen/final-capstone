<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="header.jsp"%>

<div class="content">
	<h1 class="title">Meal Plan Recipes</h1>
	<ul class="products">
		<c:forEach var="mealPlanRecipes" items="${mealPlanRecipes}">
			<c:url var="recipeImage" value="/img/${mealPlanRecipes.name}.jpg"/>
			<c:url var="stockImage" value="/img/stockPhoto.jpg"/>
				<li class="product">
					<a href="mealPlannerDetail?mealPlanId=${mealPlanId}&recipeId=${mealPlanRecipes.recipeId}">
					<h2 class="productTitle"><span>${mealPlanRecipes.name}</span></h2>
					<img src="${recipeImage}" onerror="this.src='${stockImage}'" class="productImage"/>
					</a>
				</li>
		</c:forEach>
	</ul>
	
	<form action="/capstone/${currentUser}/shoppingList?planId=${mealPlanId}" method="GET">
		<input type="hidden" name="mealPlanId" value="${mealPlanId}">
		<button type="submit" id="shoppingList" >Shopping List</button>
	</form>
	<br>
	<form action="/capstone/${currentUser}/mealPlanDeleted?mealPlanId=${mealPlanId}" method="GET">
		<input type="hidden" name="mealPlanId" value="${mealPlanId}">
		<button type="submit" id="deletePlan" >Delete Plan</button>
	</form>
	<br>
	<form action="/capstone/${currentUser}/mealPlanCleared?mealPlanId=${mealPlanId}" method="GET">
		<input type="hidden" name="mealPlanId" value="${mealPlanId}">
		<button type="submit" id="clearPlan" >Clear Meal Plan</button>
	</form>
	<br>
	<br>


<%@ include file="footer.jsp"%>