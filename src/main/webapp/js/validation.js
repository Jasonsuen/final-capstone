	$(document).ready(function () {
		debug:true,
		$('.login').validate({
			
			rules : {
				username : {
					required : true
				},
				password : {
					required : true
				}
			},
			messages:{
				username:{
					required:"Username can't be left blank",
					email:"Must be a valid email address"
				},
				password:{
					required:"Password cannot be empty"
				}
				
			},
			
			errorClass : "error"
		});
	});