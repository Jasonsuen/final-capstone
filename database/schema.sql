-- *************************************************************************************************
-- This script creates all of the database objects (tables, sequences, etc) for the database
-- *************************************************************************************************

BEGIN;

-- CREATE statements go here
CREATE TABLE recipe (
	recipe_id serial NOT NULL,
	name varchar(64) NOT NULL,
	description varchar(1008) NOT NULL,
	cook_time integer NOT NULL,
	servings integer NOT NULL,
	healthy boolean DEFAULT false NOT NULL,
	paleo boolean DEFAULT false NOT NULL,
	vegetarian boolean DEFAULT false NOT NULL,
	owner varchar(24) NOT NULL,
	CONSTRAINT pk_recipe_recipe_id PRIMARY KEY (recipe_id)
);

CREATE TABLE ingredients (
	ingredients_id serial NOT NULL,
	ingredients varchar(1008) NOT NULL,
	CONSTRAINT pk_ingredients_ingredients_id PRIMARY KEY (ingredients_id)
);

CREATE TABLE directions (
	directions_id serial NOT NULL,
	directions varchar(1512) NOT NULL,
	CONSTRAINT pk_directions_directions_id PRIMARY KEY (directions_id)
);

CREATE TABLE users (
	username varchar(24) NOT NULL,
	password varchar(24) NOT NULL,
	first_name varchar(24) NOT NULL,
	last_name varchar(24) NOT NULL,
	salt varchar(256) NOT NULL,
	CONSTRAINT pk_users_username PRIMARY KEY (username)
);

CREATE TABLE saved_recipes (
	recipe_id integer NOT NULL,
	username varchar(24) NOT NULL,
	CONSTRAINT pk_saved_recipe_recipe_id_username PRIMARY KEY (recipe_id, username)
);

CREATE TABLE meal_plans_recipes (
	plans_id integer NOT NULL,
	recipe_id integer NOT NULL,
	CONSTRAINT pk_meal_plans_recipes_plans_id_recipe_id PRIMARY KEY (plans_id, recipe_id)
);

CREATE TABLE meal_plan (
	plans_id serial NOT NULL,
	name varchar(64) NOT NULL,
	CONSTRAINT pk_meal_plan_plans_id PRIMARY KEY (plans_Id)
);

CREATE TABLE meal_plans_users (
	plans_id integer NOT NULL,
	username varchar(24) NOT NULL,
	CONSTRAINT pk_meal_plan_users_plans_id_username PRIMARY KEY (plans_id, username)
);

COMMIT;